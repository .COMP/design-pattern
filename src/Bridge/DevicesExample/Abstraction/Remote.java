package Bridge.DevicesExample.Abstraction;

public interface Remote {
  void power();

  void volumeDown();

  void volumeUp();

  void channelDown();

  void channelUp();
}
