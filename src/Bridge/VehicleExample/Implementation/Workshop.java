package Bridge.VehicleExample.Implementation;

// Implementor for bridge pattern
public interface Workshop
{
  abstract public void work();
}
