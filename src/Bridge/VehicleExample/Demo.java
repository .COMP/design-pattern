package Bridge.VehicleExample;

import Bridge.VehicleExample.Abstraction.Bike;
import Bridge.VehicleExample.Abstraction.Vehicle;
import Bridge.VehicleExample.Abstraction.Car;
import Bridge.VehicleExample.Implementation.Assemble;
import Bridge.VehicleExample.Implementation.Produce;

public class Demo {
  public static void main(String[] args) {
    Vehicle vehicle1 = new Car(new Produce(), new Assemble());
    vehicle1.manufacture();
    Vehicle vehicle2 = new Bike(new Produce(), new Assemble());
    vehicle2.manufacture();
  }
}
