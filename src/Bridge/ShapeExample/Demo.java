package Bridge.ShapeExample;

import Bridge.ShapeExample.Abstraction.Circle;
import Bridge.ShapeExample.Abstraction.Rectangle;
import Bridge.ShapeExample.Abstraction.Shape;
import Bridge.ShapeExample.Abstraction.Square;
import Bridge.ShapeExample.Implementation.BlueColor;
import Bridge.ShapeExample.Implementation.RedColor;

public class Demo {
  public static void main(String[] args) {
    Shape[] shapes = {
      new Circle(new RedColor()),
      new Square(new BlueColor()),
      new Rectangle(new RedColor()),
    };

    for(Shape shape : shapes)
      shape.draw();
  }
}
