package Bridge.ShapeExample.Implementation;

public class BlueColor implements Color{
  @Override
  public void applyColor() {
    System.out.println("Blue Color");
  }
}
