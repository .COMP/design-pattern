package Bridge.ShapeExample.Implementation;

public class RedColor implements Color{
  @Override
  public void applyColor() {
    System.out.println("Red Color");
  }
}
