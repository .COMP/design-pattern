package Bridge.ShapeExample.Implementation;

public interface Color {
  void applyColor();
}
