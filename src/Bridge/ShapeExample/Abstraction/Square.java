package Bridge.ShapeExample.Abstraction;

import Bridge.ShapeExample.Implementation.Color;

public class Square extends Shape {
  public Square(Color color) {
    super(color);
  }

  @Override
  public void draw() {
    System.out.print("Draw Square ");
    color.applyColor();
  }
}
