package Bridge.ShapeExample.Abstraction;

import Bridge.ShapeExample.Implementation.Color;

public class Circle extends Shape {
  public Circle(Color color) {
    super(color);
  }

  @Override
  public void draw() {
    System.out.print("Draw Circle ");
    color.applyColor();
  }
}
