package Bridge.ShapeExample.Abstraction;

import Bridge.ShapeExample.Implementation.Color;

public class Rectangle extends Shape {
  public Rectangle(Color color) {
    super(color);
  }

  @Override
  public void draw() {
    System.out.print("Draw Rectangle ");
    color.applyColor();
  }
}
