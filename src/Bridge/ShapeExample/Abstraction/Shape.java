package Bridge.ShapeExample.Abstraction;

import Bridge.ShapeExample.Implementation.Color;

public abstract class Shape {
  Color color;

  public Shape(Color color) {
    this.color = color;
  }

  public abstract void draw();
}
