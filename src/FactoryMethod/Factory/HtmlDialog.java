package FactoryMethod.Factory;


import FactoryMethod.Buttons.Button;
import FactoryMethod.Buttons.HtmlButton;

/**
 * HTML Dialog will produce HTML buttons.
 */
public class HtmlDialog extends Dialog {

  @Override
  public Button createButton() {
    return new HtmlButton();
  }
}
