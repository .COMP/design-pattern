package FactoryMethod.Factory;

import FactoryMethod.Buttons.Button;
import FactoryMethod.Buttons.WindowsButton;

/**
 * Windows Dialog will produce Windows buttons.
 */
public class WindowsDialog extends Dialog {

  @Override
  public Button createButton() {
    return new WindowsButton();
  }
}
