package AbstractFactory.Factory;


import AbstractFactory.Model.Button.Button;
import AbstractFactory.Model.CheckBox.Checkbox;

/**
 * Abstract factory knows about all (abstract) product types.
 */
public interface GUIFactory {
  Button createButton();
  Checkbox createCheckbox();
}
