package AbstractFactory.Factory;

import AbstractFactory.Model.Button.Button;
import AbstractFactory.Model.Button.MacButton;
import AbstractFactory.Model.CheckBox.Checkbox;
import AbstractFactory.Model.CheckBox.MacCheckbox;

/**
 * Each concrete factory extends basic factory and responsible for creating
 * products of a single variety.
 */

public class MacFactory implements GUIFactory {
  @Override
  public Button createButton() {
    return new MacButton("Clicked!", "This is my title");
  }

  @Override
  public Checkbox createCheckbox() {
    return new MacCheckbox();
  }
}
