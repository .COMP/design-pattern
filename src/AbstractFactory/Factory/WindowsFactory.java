package AbstractFactory.Factory;

import AbstractFactory.Model.Button.Button;
import AbstractFactory.Model.Button.WindowsButton;
import AbstractFactory.Model.CheckBox.Checkbox;
import AbstractFactory.Model.CheckBox.WindowsCheckbox;

/**
 * Each concrete factory extends basic factory and responsible for creating
 * products of a single variety.
 */
public class WindowsFactory implements GUIFactory {

  @Override
  public Button createButton() {
    return new WindowsButton("Clicked!", "This is my title");
  }

  @Override
  public Checkbox createCheckbox() {
    return new WindowsCheckbox();
  }
}
