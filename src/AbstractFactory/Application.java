package AbstractFactory;

import AbstractFactory.Factory.GUIFactory;
import AbstractFactory.Model.Button.Button;
import AbstractFactory.Model.CheckBox.Checkbox;

/**
 * Factory users don't care which concrete factory they use since they work with
 * factories and products through abstract interfaces.
 */
public class Application {
  private Button button;
  private Checkbox checkbox;

  public Application(GUIFactory factory) {
    button = factory.createButton();
    checkbox = factory.createCheckbox();
  }

  public void paint() {
    button.paint();
    checkbox.paint();
  }
}
