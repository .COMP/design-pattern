package AbstractFactory.Model.Button;

/**
 * All products families have the same varieties (MacOS/Windows).
 *
 * This is a MacOS variant of a button.
 */

public class MacButton implements Button {
  private  String onClick;
  private  String title;

  public MacButton(String onClick, String title) {
    this.onClick = onClick;
    this.title = title;
  }

  @Override
  public void paint() {
    System.out.printf("You have created MacOSButton With onClick %s And Title %s", this.onClick, this.title);
    System.out.println();

  }

  public String getOnClick() {
    return onClick;
  }

  public void setOnClick(String onClick) {
    this.onClick = onClick;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
