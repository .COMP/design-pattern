package Decorator.DataSourceExample.Decorator;

public interface DataSource {
  void writeData(String data);

  String readData();
}
