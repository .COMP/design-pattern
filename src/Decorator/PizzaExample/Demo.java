package Decorator.PizzaExample;

import Decorator.PizzaExample.Decorator.Pizza;
import Decorator.PizzaExample.Decorator.PizzaDecorator;
import Decorator.PizzaExample.Concrete.DeepFried;
import Decorator.PizzaExample.Concrete.DoubleCheese;

public class Demo {
  public static void main(String[] args) {
    PizzaDecorator deep_fried_with_double_cheese = new DeepFried(new DoubleCheese(new Pizza()));
    deep_fried_with_double_cheese.prepare();
  }
}
