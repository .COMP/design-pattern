package Decorator.PizzaExample.Decorator;

public class PizzaDecorator implements Item {
  Item pizza;

  public PizzaDecorator(Item pizza) {
    this.pizza = pizza;
  }

  @Override
  public void prepare() {
    pizza.prepare();
  }
}
