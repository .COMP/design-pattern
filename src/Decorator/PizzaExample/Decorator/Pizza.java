package Decorator.PizzaExample.Decorator;

import Decorator.PizzaExample.Decorator.Item;

public class Pizza implements Item {
  @Override
  public void prepare() {
    System.out.println("Pizza");
  }
}
