package Decorator.PizzaExample.Decorator;

public interface Item {
  void prepare();
}
