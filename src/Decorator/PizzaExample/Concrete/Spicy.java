package Decorator.PizzaExample.Concrete;

import Decorator.PizzaExample.Decorator.PizzaDecorator;
import Decorator.PizzaExample.Decorator.Item;

public class Spicy extends PizzaDecorator {
  public Spicy(Item pizza) {
    super(pizza);
  }

  @Override
  public void prepare() {
    super.prepare();
    System.out.println(" + Spicy");
  }
}
